# Hello this is your new site

## Fork this repository

Fork this repo as `yourname.gitlab.io`, to use custom domain.

1. Edit [config.ts](src/.vitepress/config.ts) and set site name and info.
2. Edit main page, see: [index.md](src/index.md)
3. Edit other pages, see: [examples/](src/examples/01-markdown.md)
4. …
5. PROFIT

## To use locally

Run

```
corepack enable
corepack prepare pnpm@latest --activate
pnpm install
```

And then

```
pnpm dev
```

This is [vitepress](https://vitepress.dev/guide/what-is-vitepress)
