import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "My Awesome Project",
  description: "A VitePress Site",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Examples', link: '/examples/01-markdown' }
    ],

    sidebar: [
      {
        text: 'Examples',
        items: [
          { text: 'Markdown Examples', link: '/examples/01-markdown' },
          { text: 'Runtime API Examples', link: '/examples/02-api' }
        ]
      }
    ],

    socialLinks: [
      { icon: 'mastodon', link: 'https://twiukraine.com/@partizan' }
    ]
  },
  base: '/',
  outDir: '../public',
})
